# Procedurally Generated Tree Mesh

## Installation

The material *TreeMaterial* must be attached to MeshRenderer of the tree GameObject.
Then, add *TreeMesh* script to the tree GameObject.
The script *MeshBuilder* needs to be attached to a GameObject.

## Functions

Branch length : https://www.desmos.com/calculator/gg3aein47q

Branch angle : https://www.desmos.com/calculator/3gd91vzlow

Number of branches : https://www.desmos.com/calculator/j4yic7kosd