﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class TreeMesh : MonoBehaviour
{
    public MeshFilter meshFilter;

    public int seed;

    private const float minHeight = 1;
    private const float minWidth = 1;
    private const float minAngle = -70;
    private const float maxAngle = 70;
    private const float minLife = 1;

    [Range(minHeight, 200)]
    public float height;
    [Range(minWidth, 200)]
    public float width;
    [Range(0, 100)]
    public float leavesQuantity;
    [Range(1, 3)]
    public int minBranches;
    [Range(0.001f, 1)]
    public float branchRepartition;
    [Range(1, 30)]
    public int branchAmount;
    [Range(0, 10)]
    public float branchDeviation;
    [Range(0, 1)]
    public float branchLifeTransfer;
    [Range(10, 100)]
    public float baseBranchLength;
    [Range(4, 30)]
    public int nbPointsPerCircle;
    public bool withLeaves;

    private Mesh mesh;
    private TreeBranch root;
    private bool rendering;

    private class TreeBranch
    {
        public Vector3 rotation;
        public float length;
        public float radius;
        public List<TreeBranch> branches;


        public TreeBranch()
        {
            rotation = new Vector3(0, 0);
            length = 0;
            radius = 0;
            branches = new List<TreeBranch>();
        }

        public void Render(out Vector3[] vertices, out int[] triangles, out Vector2[] uvs, int nbPointsPerCircle, Matrix4x4 state, bool withLeaves, float leavesQuantity)
        {
            MeshBuilder.instance.CreateCylinder(radius, length, state, out vertices, out triangles, out uvs, nbPointsPerCircle);

            for (int i = 0; i < uvs.Length; i++)
                uvs[i].x = 0.01f + 0.48f * uvs[i].x;

            if (branches.Count > 0)
            {
                state *= Matrix4x4.Translate(new Vector3(0, length, 0));

                Vector3[] childVertices;
                int[] childTriangles;
                Vector2[] childUV;
                Matrix4x4 childState;

                for(int i = 0; i < branches.Count; i++)
                {
                    childState = state * Matrix4x4.Rotate(Quaternion.Euler(rotation));
                    branches[i].Render(out childVertices, out childTriangles, out childUV, nbPointsPerCircle, childState, withLeaves, leavesQuantity);
                    MeshBuilder.instance.ConcatMeshes(vertices, triangles, uvs, childVertices, childTriangles, childUV, out vertices, out triangles, out uvs);
                }
            } else
            {
                if (withLeaves)
                {
                    leavesQuantity *= Random.Range(0.1f, 1.9f);
                    if (leavesQuantity > length)
                        leavesQuantity = length;

                    state *= Matrix4x4.Translate(new Vector3(0, length - leavesQuantity, 0));

                    Vector3[] sVertices;
                    int[] sTriangles;
                    Vector2[] sUV;

                    Vector3 curPos = state.MultiplyPoint3x4(Vector3.zero);
                    Matrix4x4 leavesState = Matrix4x4.Translate(curPos);

                    MeshBuilder.instance.CreateEllipsoid(leavesQuantity, leavesQuantity, leavesState, out sVertices, out sTriangles, out sUV, 10, 10);

                    for (int i = 0; i < sUV.Length; i++)
                        sUV[i].x = 0.51f + sUV[i].x * 0.49f;

                    MeshBuilder.instance.ConcatMeshes(vertices, triangles, uvs, sVertices, sTriangles, sUV, out vertices, out triangles, out uvs);
                }
            }
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        mesh = meshFilter.mesh;
        if(!mesh)
            mesh = GetComponent<MeshFilter>().mesh;
        if (!mesh)
            return;

        if(seed < 0)
            seed = (int)(Time.time * 1000000);

        rendering = true;
        GenerateTree(seed);
        RenderTree(nbPointsPerCircle);
        rendering = false;
    }

    private int BranchNumber(float parentLife, float branchAmount, float branchRepartition, float branchDeviation, int minBranches)
    {
        float nbBranchesEvaluation = branchAmount * Mathf.Exp(-1.0f / (branchRepartition) * Mathf.Sqrt(parentLife)) + minBranches;
        float branchAmountDeviation = branchDeviation * Mathf.Sqrt(branchAmount) * Mathf.Exp(-Mathf.Sqrt(2 * (parentLife + 1)));
        int nbBranches = Mathf.RoundToInt(Random.Range(nbBranchesEvaluation - branchAmountDeviation, nbBranchesEvaluation + branchAmountDeviation));
        if (nbBranches < minBranches)
            nbBranches = minBranches;
        return nbBranches;
    }

    private float NextBranchAngle(float life, float treeRatio, float minAngle, float maxAngle)
    {
        float rValue = Random.Range(-10.0f, 10.0f);
        float expRatio = Mathf.Exp(-treeRatio);
        return (maxAngle - minAngle) / (1.0f + Mathf.Exp(-expRatio * rValue)) + minAngle;
    }

    private float NextBranchLife(int branchIndex, float life, float remainingLife, int nbBranches, float minLife)
    {
        if (branchIndex >= nbBranches - 1) return remainingLife;

        float maxDeviation = Mathf.Sqrt(life / nbBranches);
        float deviation = Random.Range(-maxDeviation, maxDeviation);

        float availableLife = remainingLife - minLife * (nbBranches - branchIndex);

        float avg = availableLife / nbBranches + minLife;

        float bLife = avg + deviation;

        if (bLife > availableLife) bLife = availableLife;
        if (bLife > remainingLife) bLife = remainingLife;
        if (bLife < minLife) bLife = minLife;

        return bLife;
    }

    private float BranchLength(float life, float pLife, float baseBranchLength)
    {
        if (pLife == 0) return baseBranchLength;  
        return baseBranchLength * (1 - Mathf.Exp(-1.0f / (1.0f + life / pLife)));
    }

    private float BranchRadius(float life)
    {
        return Mathf.Log(Mathf.Sqrt(life) + 1);
    }

    private void GenerateBranches(TreeBranch parent, float pLife, int seed)
    {
        if (pLife <= 1) return;

        int nbBranches = BranchNumber(pLife, branchAmount, branchRepartition, branchDeviation, minBranches);

        float ratio = height / width;
        float life, remainingLife = pLife * branchLifeTransfer;

        for(int i = 0; i < nbBranches; i++)
        {
            TreeBranch newBranch = new TreeBranch();

            life = NextBranchLife(i, pLife, remainingLife, nbBranches, minLife);
            remainingLife -= life;

            newBranch.rotation.x = NextBranchAngle(pLife, ratio, minAngle, maxAngle);
            newBranch.rotation.z = NextBranchAngle(pLife, ratio, minAngle, maxAngle);

            newBranch.length = BranchLength(life, pLife, baseBranchLength);
            newBranch.radius = BranchRadius(life);

            if (double.IsNaN(newBranch.radius)) return;

            GenerateBranches(newBranch, life, seed);

            parent.branches.Add(newBranch);
        }
    }

    private void GenerateTree(int seed)
    {
        if (height < minHeight) height = minHeight;
        if (width < minWidth) width = minWidth;


        root = new TreeBranch();
        Random.InitState(seed);
        float life = height * width * Random.Range(0.9f, 1.1f);
        root.length = BranchLength(life, 0, baseBranchLength);
        root.radius = BranchRadius(life);
        GenerateBranches(root, life, seed);
    }

    List<T> ArrayToList<T>(T[] array)
    {
        List<T> res = new List<T>(array.Length);
        for (int i = 0; i < array.Length; i++)
            res.Add(array[i]);
        return res;
    }

    private void RenderTree(int nbPointsPerCircle)
    {
        if (!mesh) return;

        mesh.Clear();

        Vector3[] vertices;
        int[] triangles;
        Vector2[] uvs;
        
        root.Render(out vertices, out triangles, out uvs, nbPointsPerCircle, Matrix4x4.identity, withLeaves, leavesQuantity);

        mesh.SetVertices(ArrayToList(vertices));
        mesh.SetUVs(0, ArrayToList(uvs));
        mesh.SetTriangles(ArrayToList(triangles), 0);

        mesh.RecalculateBounds();
        mesh.RecalculateNormals();
        mesh.RecalculateTangents();
    }


    private void OnValidate()
    {
        if (seed < 0)
            seed = (int)(Time.time * 1000000);

        if (rendering) return;

        rendering = true;
        GenerateTree(seed);
        RenderTree(nbPointsPerCircle);
        rendering = false;
    }
}
