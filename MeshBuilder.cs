﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshBuilder : MonoBehaviour
{
    public static MeshBuilder instance;
    // Start is called before the first frame update
    void Awake()
    {
        instance = this;
    }

    public void CreateCylinder(float radius, float length, Matrix4x4 state, out Vector3[] vertices, out int[] triangles, out Vector2[] uv, int nbPointsPerCircle = 20, int nbLines = 2)
    {
        if (nbLines < 2) nbLines = 2;

        int nbPoints = nbLines * nbPointsPerCircle;

        vertices = new Vector3[nbPoints];
        // Each vertex is the origin of a face (2 triangles = 6 points) except for the last line 
        triangles = new int[(nbPoints - nbPointsPerCircle) * 6];
        uv = new Vector2[nbPoints];

        float rStep = 2.0f * Mathf.PI / nbPointsPerCircle;
        float lStep = length / (nbLines - 1);

        Vector2 uvStep = new Vector2(1.0f / nbPointsPerCircle, 1.0f / (nbLines - 1));

        int index, triIndex, i, j;
        for (i = 0; i < nbPointsPerCircle; i++)
        {
            for(j = 0; j < nbLines; j++)
            {
                // Index of the vertex in the array
                index = i + j * nbPointsPerCircle;

                // Position of the vertex
                vertices[index].x = radius * Mathf.Cos(rStep * i);
                vertices[index].y = lStep * j;
                vertices[index].z = radius * Mathf.Sin(rStep * i);
            
                vertices[index] = state.MultiplyPoint3x4(vertices[index]);

                uv[index].x = uvStep.x * i;
                uv[index].y = uvStep.y * j;
            
                // If the vertex is on the last line, there is no face
                if (j == nbLines - 1) continue;

                triIndex = index * 6;   // Index in the triangles array

                // Indices of the 4 corner of the face
                int topLeft = index;
                int topRight = index + 1;
                int bottomLeft = index + nbPointsPerCircle;
                int bottomRight = index + nbPointsPerCircle + 1;

                // Make sure that vertices are on the same line
                if (topRight >= (j + 1) * nbPointsPerCircle) topRight = j * nbPointsPerCircle;
                if (bottomRight >= (j + 2) * nbPointsPerCircle) bottomRight = (j + 1) * nbPointsPerCircle;

                // Place triangles
                triangles[triIndex + 0] = topLeft;
                triangles[triIndex + 1] = bottomLeft;
                triangles[triIndex + 2] = topRight;

                triangles[triIndex + 3] = topRight;
                triangles[triIndex + 4] = bottomLeft;
                triangles[triIndex + 5] = bottomRight;
            }
        }
    }

    public void CreateEllipsoid(float width, float height, Matrix4x4 state, out Vector3[] vertices, out int[] triangles, out Vector2[] uv, int nbColumns = 20, int nbLines = 20)
    {
        CreateCylinder(width, height, Matrix4x4.identity, out vertices, out triangles, out uv, nbColumns, nbLines);

        float step = Mathf.PI / (nbLines - 1), r;

        int i, j, index;
        for(i = 0; i < nbLines; i++)
        {
            r = Mathf.Sin(step * i);
            for(j = 0; j < nbColumns; j++)
            {
                index = j + i * nbColumns;
                vertices[index].x *= r;
                vertices[index].z *= r;
            }
        }

        for (i = 0; i < vertices.Length; i++)
            vertices[i] = state.MultiplyPoint3x4(vertices[i]);
    }

    public void ConcatMeshes(Vector3[] v1, int[] t1, Vector2[] uv1,
                                Vector3[] v2, int[] t2, Vector2[] uv2,
                                out Vector3[] vertices, out int[] triangles, out Vector2[] uv)
    {
        vertices = new Vector3[v1.Length + v2.Length];
        triangles = new int[t1.Length + t2.Length];
        uv = new Vector2[uv1.Length + uv2.Length];

        int i;
        for (i = 0; i < v1.Length; i++)     vertices[i]     = v1[i];
        for (i = 0; i < t1.Length; i++)     triangles[i]    = t1[i];
        for (i = 0; i < uv1.Length; i++)    uv[i]           = uv1[i];

        for (i = 0; i < v2.Length; i++)     vertices[v1.Length + i]     = v2[i];
        for (i = 0; i < t2.Length; i++)     triangles[t1.Length + i]    = t2[i] + v1.Length;
        for (i = 0; i < uv2.Length; i++)    uv[uv1.Length + i]          = uv2[i];

    }
}
